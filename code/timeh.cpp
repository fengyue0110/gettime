#include "timeh.h"

namespace timeh{
    uint64_t gets()
    {
        timespec ts;
        clock_gettime(CLOCK_THREAD_CPUTIME_ID,&ts);
        return (uint64_t)ts.tv_sec;
    }

    uint64_t getms()
    {
        timespec ts;
        clock_gettime(CLOCK_THREAD_CPUTIME_ID,&ts);
        return (uint64_t)ts.tv_sec*1000+(uint64_t)ts.tv_nsec/1000000;
    }

    uint64_t getus()
    {
        timespec ts;
        clock_gettime(CLOCK_THREAD_CPUTIME_ID,&ts);
        return (uint64_t)ts.tv_sec*1000000+(uint64_t)ts.tv_nsec/1000;
    }

    uint64_t getns()
    {
        timespec ts;
        clock_gettime(CLOCK_THREAD_CPUTIME_ID, &ts);
        return (uint64_t)ts.tv_sec*1000000000+(uint64_t)ts.tv_nsec;
    }

    void timeinit()
    {
        timespec ts;
        ts.tv_sec = 0;
        ts.tv_nsec = 0;
        clock_settime(CLOCK_THREAD_CPUTIME_ID, &ts);
    }
}