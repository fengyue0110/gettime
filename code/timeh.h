#include <iostream>
#include <sys/time.h>
using namespace std;

namespace timeh{
    uint64_t gets();
    uint64_t getms();
    uint64_t getus();
    uint64_t getns();
    void timeinit();
}